package com.icey.crudtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@EnableAutoConfiguration
@ComponentScan(basePackages={"com.icey.crudtest"})
@EnableJpaRepositories(basePackages="com.icey.crudtest.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="com.icey.crudtest.entities")
@SpringBootApplication
public class CrudtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudtestApplication.class, args);
	}

}
